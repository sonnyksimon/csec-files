Q: What reasons does Pa Ben give for fretting about him in Africa?
A: Pa Ben states that: - a lion or tiger may eat him raw
		       - the "savages in the bushes" may eat him as stew.

Q: Why does Pa Ben call Africa the dark continent?
A: Because it consists of blacks and is the homeland of obeah.

Q: Why does Miss Aggy believe that the woman in the photograph has set obeah on Len?
A: Because she blindly believes that there is no way Len could marry a black woman out of his own free will.

Q: Why does Miss Aggy urge Len to give something to Pearl? What does her request tell us about the differing    situations of Len and Pearl?
A: Miss Aggy wants to show Pearl the difference between her and Len. Even though they grew in the same     circumstances, Len is now clearly more "wealthy and rich" than Pearl.

Q: Pa Ben seems to think it more urgent that he get a few starapples for Len than receive the gift that Len     has brought him. How does this detail affect your impression of Pa Ben?
A: Due to this, Pa Ben is presented as someone truly courteous and give a "peek" of the friendship between   Len and Pa Ben.

Q: Why does Miss Aggy think Len will cure her cough?
A: By hearing about the "Dr" behind Len's names, she assumes him to be a medical doctor.

Q: What is Lois complaining about? Indicate evidence in the script that support you answer.
A: She is complaining about the lack of sexual activity
  (pg.26."has a woman ever lost her husband to a book?").

Q: Why does Miss Aggy call "Hold dog!"?
A: She wants to imply that Lois is a dog by using advantage of the sign - "Beware of dogs".

Q: Identify two things Miss Agyy does or says which might cause offence to Lois.
A: - She says that Len is not eating well
   - She starts to inspect and clean the furniture

Q: Identify the moment when Lois hits back at Miss Aggy.
A: "I'll go clean the shit from the dog house" is the moment.